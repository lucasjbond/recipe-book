import { Injectable } from "@angular/core";
import { Ingredient } from "src/app/shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list/shopping-lilst.service";
import { Recipe } from "./recipe.model";

@Injectable()
export class RecipeService {
    constructor(private shoppingListService: ShoppingListService) {}

    private recipes: Recipe[] = [
        new Recipe(
            'Test Recipe', 
            'This is a test', 
            'https://cdn.loveandlemons.com/wp-content/uploads/2020/03/pantry-recipes-2.jpg',
            [
                new Ingredient('Greens', 1),
                new Ingredient('Beans', 2),
                new Ingredient('Tomoatoes', 3)
            ]),
        new Recipe(
            'Test Recipe 2', 
            'This is a test 2',
            'https://cdn.loveandlemons.com/wp-content/uploads/2020/03/pantry-recipes-2.jpg',
            [
                new Ingredient('Po', 1),
                new Ingredient('tat', 1),
                new Ingredient('O', 1)
            ])
    ];

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addIngredientsToList(ingredients: Ingredient[]) {
        this.shoppingListService.addIngredients(ingredients);
    }

}